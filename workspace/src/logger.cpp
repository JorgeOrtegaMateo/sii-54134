//Jorge Ortega Mateo

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	char buffer[128];
	if (mkfifo("Tuberia",0777)!=0)
	printf ("Hubo un problema al crear la pipe\n");

	int mipipe=open("Tuberia", O_RDONLY);
	
	while(read(mipipe, buffer, 128))
	{
		printf("%s", buffer);
	}

	close(mipipe);
	unlink("Tuberia");
	return 0;
}
